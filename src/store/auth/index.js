import firebase from "firebase";
import "firebase/storage";
import { v4 as uuidv4 } from "uuid";

const config = {
  apiKey: "AIzaSyDEHEAVuLRBQ2v06F3EVozR8SvqllLvUTk",
  authDomain: "cloth-hub.firebaseapp.com",
  databaseURL: "https://cloth-hub.firebaseio.com",
  projectId: "cloth-hub",
  storageBucket: "cloth-hub.appspot.com",
  messagingSenderId: "93374105239"
};
firebase.initializeApp(config);

const db = firebase.firestore();
const storage = firebase.storage();

const state = {
  user: "",
  popularShops: [],
  shop: null,
  shopProducts: [],
  product: null,
  cart: null,
  activation: null,
  subscription: null,
  allActivation: [],
  mySubscribes: [],
  productsInSub: [],
  ordersInShop: []
};

const getters = {};

const actions = {
  register: async ({ commit }, payload) => {
    console.log(payload);
    let response;
    await firebase
      .auth()
      .createUserWithEmailAndPassword(payload.email, payload.password)
      .then(userCredential => {
        console.log(userCredential.user.uid);
        if (!!userCredential.user.uid) {
          db.collection("users")
            .doc(userCredential.user.uid)
            .set({
              id: userCredential.user.uid,
              email: payload.email,
              createdAt: Date.now(),
              updatedAt: Date.now(),
              firstname: "",
              lastname: "",
              address: "",
              type: 0,
              isMerchant: false,
              isActive: true
            });
        }
        response = true;
      })
      .catch(error => {
        console.log(error);
        response = false;
      });
    return response;
  },
  login: async ({ commit }, payload) => {
    console.log(payload);
    let response;
    await firebase
      .auth()
      .signInWithEmailAndPassword(payload.email, payload.password)
      .then(res => {
        let user = {
          uid: res.user.uid,
          email: res.user.email
        };
        commit("SET_USER", user);
        response = true;
        window.location.reload();
      })
      .catch(error => {
        console.log(error);
        response = false;
      });
    return response;
  },
  logout: async ({ commit }, payload) => {
    localStorage.removeItem("email");
    localStorage.removeItem("uid");
  },
  updateUser: async ({ commit }, user) => {
    console.log(user);
    db.collection("users")
      .doc(user.id)
      .update({
        firstname: user.firstname,
        lastname: user.lastname,
        address: user.address
      });
  },
  getUserData: async ({ commit }, payload) => {
    console.log("payload", payload);
    await db
      .collection("users")
      .where("id", "==", payload)
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          console.log(doc.id, " => ", doc.data());
          let data = doc.data();
          let user = {
            uid: data.id,
            email: data.email,
            createdAt: data.createdAt,
            updatedAt: data.updatedAt,
            firstname: data.firstname,
            lastname: data.lastname,
            address: data.address,
            type: data.type,
            isMerchant: data.isMerchant,
            isActive: data.isActive,
            sid: data.sid
          };
          commit("SET_USER", user);
        });
      })
      .catch(error => {
        console.log("Error getting documents: ", error);
      });
  },
  addProduct: async ({ state, commit }, payload) => {
    console.log(state.shop);
    const docId = await db
      .collection("products")
      .add({
        id: uuidv4(),
        uid: localStorage.getItem("uid"),
        sid: state.shop.id,
        name: payload.name,
        description: payload.description,
        price: payload.price,
        stock: payload.stock,
        type: payload.type,
        sold: 0,
        showBanner: true,
        createdAt: Date.now(),
        updatedAt: Date.now()
      })
      .then(docRef => {
        console.log(docRef.id);
        return docRef.id;
      })
      .catch(error => {
        console.error("Error adding document: ", error);
      });
    const index = await payload.images.map(image => {
      const ref = storage
        .ref()
        .child(uuidv4() + "_" + localStorage.getItem("uid"));
      ref.put(image).then(snapshot => {
        snapshot.ref.getDownloadURL().then(res => {
          db.collection("products")
            .doc(docId)
            .update({
              images: firebase.firestore.FieldValue.arrayUnion(res)
            });
        });
      });
    });
  },
  getShopPopulars: async ({ commit }) => {
    commit("CLEAR_POPULAR_SHOP");
    let shops = [];
    await db
      .collection("shops")
      .where("rating", ">", 4)
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          let products = [];
          let shopData = doc.data();
          db.collection("products")
            .where("sid", "==", shopData.id)
            .get()
            .then(querySnapshot => {
              querySnapshot.forEach(doc => {
                if (products.length < 4 && doc.data().showBanner) {
                  products.push(doc.data());
                }
              });
              let shop = {
                shop: shopData,
                products: products
              };
              commit("SET_POPULAR_SHOP", shop);
            });
        });
      });
  },
  getShopDataBySid: async ({ commit }, sid) => {
    await db
      .collection("shops")
      .where("id", "==", sid)
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          commit("SET_SHOP", doc.data());
        });
      })
      .catch(error => {
        console.log("Error getting documents: ", error);
      });
  },
  getProductsByShopId: async ({ commit }, sid) => {
    let products = [];
    await db
      .collection("products")
      .where("sid", "==", sid)
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          console.log(doc.id, " => ", doc.data());
          products.push(doc.data());
        });
        commit("SET_SHOP_PRODUCT", products);
      })
      .catch(error => {
        console.log("Error getting documents: ", error);
      });
  },
  getProductById: async ({ commit }, pid) => {
    await db
      .collection("products")
      .where("id", "==", pid)
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          console.log(doc.id, " => ", doc.data());
          commit("SET_PRODUCT", doc.data());
        });
      })
      .catch(error => {
        console.log("Error getting documents: ", error);
      });
  },
  getLocalCart: async ({ commit }) => {
    commit("SET_CART", null);
    let localCart = JSON.parse(localStorage.getItem("cart"));
    await localCart.map(productCart => {
      db.collection("products")
        .where("id", "==", productCart.id)
        .get()
        .then(querySnapshot => {
          querySnapshot.forEach(doc => {
            let data = doc.data();
            data.quantity = productCart.quantity;
            data.size = productCart.size;
            commit("SET_CART", data);
          });
        })
        .catch(error => {
          console.log("Error getting documents: ", error);
        });
    });
  },
  activateMerchant: async ({ commit }, payload) => {
    const ref = storage
      .ref()
      .child(uuidv4() + "_" + localStorage.getItem("uid") + "_file");
    ref.put(payload.document[0]).then(snapshot => {
      snapshot.ref.getDownloadURL().then(res => {
        db.collection("activation").add({
          uid: localStorage.getItem("uid"),
          citizenId: payload.citizenId,
          phone: payload.phone,
          status: "wait",
          file: res,
          shopName: payload.shopName,
          shopDescription: payload.shopDescription,
          shopType: payload.shopType
        });
        console.log(res);
      });
    });
  },
  getActivation: async ({ commit }, uid) => {
    await db
      .collection("activation")
      .where("uid", "==", uid)
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          commit("SET_ACTIVATION", doc.data());
        });
      })
      .catch(error => {
        console.log("Error getting documents: ", error);
      });
  },
  getAllActivation: async ({ commit }) => {
    await db
      .collection("activation")
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          let newData = doc.data();
          newData["_id"] = doc.id;
          commit("SET_ALL_ACTIVATION", newData);
        });
      })
      .catch(error => {
        console.log("Error getting documents: ", error);
      });
  },
  updateShop: async ({ commit }, payload) => {
    console.log(payload);
    const shopId = await (
      await db
        .collection("shops")
        .where("id", "==", payload.sid)
        .get()
    ).docs[0].id;
    console.log(shopId);
    const ref = storage
      .ref()
      .child(uuidv4() + "_" + localStorage.getItem("uid") + "_shop");
    const ref_ = storage
      .ref()
      .child(uuidv4() + "_" + localStorage.getItem("uid") + "_shop");
    await ref.put(payload.shopImage[0]).then(snapshot => {
      snapshot.ref.getDownloadURL().then(res => {
        console.log(res);
        db.collection("shops")
          .doc(shopId)
          .update({
            image: res,
            // banner: payload.shopBanner,
            name: payload.shopName,
            description: payload.shopDescription,
            type: payload.shopType
          });
      });
    });
    await ref_.put(payload.shopBanner[0]).then(snapshot => {
      snapshot.ref.getDownloadURL().then(res => {
        console.log(res);
        db.collection("shops")
          .doc(shopId)
          .update({
            banner: res
          });
      });
    });
  },
  addSubscription: async ({ commit }, payload) => {
    console.log(payload);
    await db.collection("subscription").add({
      sid: state.shop.id,
      uid: localStorage.getItem("uid"),
      price: payload.price,
      quantity: payload.quantity,
      productPriceLowest: payload.productPriceLowest,
      productPriceHighest: payload.productPriceHighest,
      expireDate: payload.expireDate,
      duration: payload.duration,
      shopName: payload.shopName
    });
  },
  getSubscriptionBySid: async ({ commit }, payload) => {
    let subscriptions = [];
    await db
      .collection("subscription")
      .where("sid", "==", payload)
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          let data = doc.data();
          data.id = doc.id;
          subscriptions.push(data);
        });
        commit("SET_SUB", subscriptions);
      })
      .catch(error => {
        console.log("Error getting documents: ", error);
      });
  },
  activateShop: async ({ commit }, payload) => {
    await db
      .collection("shops")
      .add({
        name: payload.shopName,
        description: payload.shopDescription,
        createdAt: Date.now(),
        uid: payload.uid
      })
      .then(res => {
        db.collection("users")
          .doc(payload.uid)
          .update({
            isMerchant: true,
            sid: res.id
          });
        db.collection("shops")
          .doc(res.id)
          .update({
            id: res.id
          });
      });
    await db
      .collection("activation")
      .doc(payload._id)
      .update({
        status: "active"
      });
  },
  addOrder: async ({ commit }, payload) => {
    let productList = Array.from(payload);
    let allSid = await productList.map(product => product[0]);
    console.log("productList", productList);
    await db
      .collection("users")
      .where("id", "==", localStorage.getItem("uid"))
      .get()
      .then(res => {
        console.log(res.docs[0].data());
        db.collection("orders").add({
          products: JSON.stringify(productList),
          status: "packing",
          createdAt: Date.now(),
          uid: localStorage.getItem("uid"),
          sid: allSid,
          name: res.docs[0].data().firstname
            ? res.docs[0].data().firstname + " " + res.docs[0].data().lastname
            : localStorage.getItem("email"),
          address: res.docs[0].data().address
        });
      });
    productList.map(productsPerShop => {
      productsPerShop[1].map(product => {
        db.collection("products")
          .where("id", "==", product.id)
          .get()
          .then(res => {
            let newStock = res.docs[0].data().stock;
            newStock[`${product.size.toLowerCase()}`] = (
              parseInt(newStock[`${product.size.toLowerCase()}`]) -
              product.quantity
            ).toString();
            db.collection("products")
              .doc(res.docs[0].id)
              .update({
                stock: newStock,
                sold: res.docs[0].data().sold + product.quantity
              });
          });
      });
    });
  },
  subscribe: async ({ commit }, payload) => {
    const subscription = await db
      .collection("subscription")
      .doc(payload)
      .get();
    console.log("subscription", subscription.data());
    const nowDate =
      new Date().getDate() +
      "-" +
      (new Date().getMonth() + 1) +
      "-" +
      new Date().getFullYear();
    await db.collection("subscribeLog").add({
      uid: localStorage.getItem("uid"),
      startDate: nowDate,
      subId: subscription.id,
      sid: subscription.data().sid
    });
  },
  getSubscriptionById: async ({ commit }, id) => {
    const subscription = await db
      .collection("subscription")
      .doc(id)
      .get();
    return subscription.data();
  },
  getMySubscribe: async ({ commit }) => {
    let subList = [];
    await db
      .collection("subscribeLog")
      .where("uid", "==", localStorage.getItem("uid"))
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          subList.push(doc.data());
        });
      })
      .catch(error => {
        console.log("Error getting documents: ", error);
      });
    console.log("subList", subList);
    await subList.map(sub => {
      db.collection("subscription")
        .doc(sub.subId)
        .get()
        .then(querySnapshot => {
          // console.log("querySnapshot", querySnapshot.data());
          let newData = querySnapshot.data();
          newData["_id"] = querySnapshot.id;
          newData["startDate"] = sub.startDate;
          commit("SET_MY_SUB", newData);
        })
        .catch(error => {
          console.log("Error getting documents: ", error);
        });
    });
  },
  getProductsBySubId: async ({ commit }, subId) => {
    const subscription = await db
      .collection("subscription")
      .doc(subId)
      .get()
      .then(res => {
        return res.data();
      });
    // console.log("subscription", subscription);
    const products = await db
      .collection("products")
      .where("price", "<=", subscription.productPriceHighest)
      .get()
      .then(res => {
        return res.docs.map(doc => doc.data());
      });
    // console.log("products", products);
    commit(
      "SET_PRODUCTS_IN_SUB",
      products.filter(pd => pd.sid == subscription.sid)
    );
  },
  getProductByType: async ({ commit }, type) => {
    const products = await db
      .collection("products")
      .where("type", "array-contains", type)
      .get()
      .then(res => {
        return res.docs.map(doc => doc.data());
      });
    console.log("products", products);
    commit("SET_PRODUCTS_IN_SUB", products);
  },
  getOrdersBySid: async ({ commit }, sid) => {
    const orders = await db
      .collection("orders")
      .where("sid", "array-contains", sid)
      .get()
      .then(res => {
        res.docs.map(order => {
          let newData = order.data();
          newData.products = JSON.parse(newData.products);
          commit("SET_ORDERS_INSHOP", newData);
        });
        // return res.docs.map(doc => res.data());
      });
    // console.log("orders", orders);
    // commit("SET_ORDERS_INSHOP", orders);
  }
};

const mutations = {
  SET_USER: async (state, user) => {
    localStorage.setItem("email", user.email);
    localStorage.setItem("uid", user.uid);
    state.user = user;
  },
  CLEAR_POPULAR_SHOP: async state => {
    state.popularShops.splice(0, state.popularShops.length);
  },
  SET_POPULAR_SHOP: async (state, popularShops) => {
    state.popularShops.push(popularShops);
  },
  SET_SHOP: async (state, shop) => {
    state.shop = shop;
  },
  SET_SHOP_PRODUCT: async (state, products) => {
    state.shopProducts = products;
  },
  SET_PRODUCT: async (state, product) => {
    state.product = product;
  },
  SET_CART: async (state, cart) => {
    if (cart === null) {
      state.cart = [];
    } else {
      state.cart.push(cart);
    }
  },
  SET_ACTIVATION: async (state, data) => {
    state.activation = data;
  },
  SET_ALL_ACTIVATION: async (state, data) => {
    state.allActivation.push(data);
  },
  SET_SUB: async (state, data) => {
    state.subscription = data;
  },
  SET_MY_SUB: async (state, data) => {
    state.mySubscribes.push(data);
  },
  SET_PRODUCTS_IN_SUB: async (state, data) => {
    state.productsInSub = data;
  },
  SET_ORDERS_INSHOP: async (state, data) => {
    state.ordersInShop.push(data);
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
